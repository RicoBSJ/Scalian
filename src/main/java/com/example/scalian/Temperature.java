package com.example.scalian;

import java.util.*;

public class Temperature {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        // Number of values
        int n = in.nextInt();

        List<Integer> intValues = new ArrayList<>();

        int closestToZero = (n == 0) ? 0 : in.nextInt();

        for (int i = 0; i < n; i++) {
            int value = in.nextInt();

            if (Math.abs(value) < Math.abs(closestToZero) || (value == -closestToZero) && value > 0){
                closestToZero = value;
            }
            intValues.sort(new TempComparator());

            System.out.println(closestToZero);
        }
    }
    private static class TempComparator implements Comparator<Integer>{
        public int compare(Integer t1, Integer t2){
            return t1.compareTo(t2);
        }
    }
}
