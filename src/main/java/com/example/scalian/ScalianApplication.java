package com.example.scalian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScalianApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScalianApplication.class, args);
	}

}
